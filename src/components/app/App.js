import {BrowserRouter as Router, Route, Switch} from "react-router-dom";
import {lazy, Suspense} from "react";

import AppHeader from "../appHeader/AppHeader";
import Spinner from "../spinner/Spinner";

const MainPages = lazy(() => import("../pages/mainPage/MainPages")),
    ComicsPage = lazy(() => import("../pages/comicsPage/ComicsPage")),
    Page404 = lazy(() => import("../pages/404/404")),
    SingleComicLayout = lazy(() => import("../pages/singleComicLayout/SingleComicLayout")),
    SinglePageLayout = lazy((() => import("../pages/singlePageLayout"))),
    SingleCharLayout = lazy((() => import("../pages/singleCharLayout/singleCharLayout")));


const App = () => {

    return (
        <Router>
            <div className="app">
                <AppHeader/>
                <main>
                    <Suspense fallback={<Spinner/>}>
                        <Switch>
                            <Route exact path="/">
                                <MainPages/>
                            </Route>
                            <Route exact path="/comics">
                                <ComicsPage/>
                            </Route>
                            <Route exact path="/comics/:id">
                                <SinglePageLayout Component={SingleComicLayout} dataType="comic"/>
                            </Route>
                            <Route exact path="/characters/:id">
                                <SinglePageLayout Component={SingleCharLayout} dataType="character"/>
                            </Route>
                            <Route path="*">
                                <Page404/>
                            </Route>
                        </Switch>
                    </Suspense>
                </main>
            </div>
        </Router>
    )
}

export default App;