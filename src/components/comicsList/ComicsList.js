import {useState, useEffect} from "react";
import {Link} from "react-router-dom"

import useMarvelService from "../../services/MarvelService";
import Spinner from "../spinner/Spinner";
import ErrorMessage from "../errorMessage/ErrorMessage"

import './comicsList.scss';


const ComicsList = () => {

    const [comicsList, setComicsList] = useState([]);
    const [newItemLoading, setNewItemLoading] = useState(false);
    const [offset, setOffset] = useState(0);
    const [comicsEnded, setComicsEnded] = useState(false);

    const {process, setProcess, clearError, getAllComics} = useMarvelService();

    useEffect(() => {
        onRequest(offset, true);
        // eslint-disable-next-line
    }, []);

    const onRequest = (offset, initial) => {
        initial ? setNewItemLoading(false) : setNewItemLoading(true);
        getAllComics(offset)
            .then(onComicsListLoaded)
        .then(() => setProcess('success'));
    }

    const onComicsListLoaded = (newComicsList) => {
        clearError();
        let comicsEnded = false;
        if (newComicsList.length < 8) {
            comicsEnded = true;
        }

        setComicsList([...comicsList, ...newComicsList]);
        setNewItemLoading(false);
        setOffset(offset + 8);
        setComicsEnded(comicsEnded);
    }

    const setContent = () => {
        switch (process) {
            case 'waiting':
                return <Spinner/>
            case 'loading':
                return !newItemLoading ? <Spinner/> : renderItems(comicsList);
            case 'error':
                return <ErrorMessage/>;
            case 'success':
                return renderItems(comicsList);
            default:
                throw new Error('Unexpected process state');
        }
    }

    function renderItems(arr) {
        const items = arr.map((item, i) => {
            return (
                <li key={i} className="comics__item">
                    <Link aria-label="comic" to={`/comics/${item.id}`}>
                        <img src={item.thumbnail} alt={item.title} className="comics__item-img"/>
                        <div className="comics__item-name">{item.title}</div>
                        <div className="comics__item-price">{item.price}</div>
                    </Link>
                </li>
            )
        });

        return (
            <ul className="comics__grid">
                {items}
            </ul>
        )
    }

    return (
        <div className="comics__list">
            {setContent()}
            <button
                aria-label="load more"
                style={{'display': comicsEnded ? 'none' : 'block'}}
                disabled={newItemLoading}
                className="button button__main button__long"
                onClick={()=>onRequest(offset)}>
                <div className="inner">load more</div>
            </button>
        </div>
    )
}

export default ComicsList;