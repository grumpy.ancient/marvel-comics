import img from './80698-404-error.gif';
import './errorMessage.scss'

const ErrorMessage = () => {
    return (<img className="error" src={img} alt="Error"/>)
}
export default ErrorMessage;