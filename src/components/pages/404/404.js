import ErrorMessage from "../../errorMessage/ErrorMessage";
import {Link} from "react-router-dom";
import {Helmet} from "react-helmet";

import "./404.scss"

const Page404 = () => {
    return (
        <div>
            <Helmet>
                <meta
                    name="description"
                    content="There are not content" />
                />
                <title>Page not found</title>
            </Helmet>
            <ErrorMessage/>
            <Link className="back" to="/">Back to main page</Link>
        </div>
    )
}
export default Page404;