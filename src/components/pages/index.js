import MainPages from "./mainPage/MainPages";
import ComicsPage from "./comicsPage/ComicsPage";
import Page404 from "./404/404";
import SingleComicPage from "./singleComicLayout/SingleComicLayout";

export {MainPages,ComicsPage,Page404,SingleComicPage}
