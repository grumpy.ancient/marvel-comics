import {useState} from 'react';
import {Helmet} from "react-helmet";

import RandomChar from "../../randomChar/RandomChar";
import CharList from "../../charList/CharList";
import CharInfo from "../../charInfo/CharInfo";
import SearchCharacters from "../../searchCharacters/searchCharacters";
import ErrorBoundaries from "../../errorBoundaries/ErrorBoundaries";

import decoration from '../../../resources/img/vision.png';

const MainPages = () => {
    const [selectedChar, setSelectedChar] = useState(null);

    const onCharSelected = (id) => {
        setSelectedChar(id);
    }

    return (
        <>
            <Helmet>
                <meta
                    name="description"
                    content="Marvel information portal"
                />
                <title>Marvel information portal</title>
            </Helmet>
            <ErrorBoundaries>
                <RandomChar/>
            </ErrorBoundaries>
            <div className="char__content">
                <ErrorBoundaries>
                    <CharList onCharSelected={onCharSelected}/>
                </ErrorBoundaries>
                <div>
                    <ErrorBoundaries>
                        <CharInfo charId={selectedChar}/>
                    </ErrorBoundaries>
                    <ErrorBoundaries>
                        <SearchCharacters/>
                    </ErrorBoundaries>
                </div>
            </div>
            <img className="bg-decoration" src={decoration} alt="vision"/>
        </>
    )
}

export default MainPages;