import {Helmet} from "react-helmet";

import './singleCharLayout.scss';

const SingleCharLayout = ({data}) => {
    const {name, thumbnail, descriptionFull} = data;

    return (
        <div className="single-char">
            <Helmet>
                <meta
                    name="description"
                    content={name}
                />
                <title>{name}</title>
            </Helmet>
            <img src={thumbnail} alt={name} className="single-char__img"/>
            <div className="single-char__info">
                <h2 className="single-char__name">{name}</h2>
                <p className="single-char__descr">{descriptionFull}</p>
            </div>
        </div>
    )
}


export default SingleCharLayout;
