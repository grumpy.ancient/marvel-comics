import {useState, useEffect} from 'react';
import {useParams} from "react-router-dom";

import useMarvelService from "./../../services/MarvelService";
import setContent from "../../utils/setContent";
import AppBanner from "./../appBanner/AppBanner";

const SinglePageLayout = ({Component, dataType}) => {
    const {id} = useParams();
    const [data, setData] = useState(null);
    const {process, setProcess, getComic, getCharacter, clearError} = useMarvelService();

    useEffect(() => {
        updateData();
        // eslint-disable-next-line
    }, [id]);


    const updateData = () => {
        clearError();

        switch (dataType) {
            case 'comic':
                getComic(id).then(onDataLoaded).then(() => setProcess('success'));
                break;
            case 'character':
                getCharacter(id).then(onDataLoaded).then(() => setProcess('success'));
                break;
            default:
                throw new Error('Unknown dataType');
        }
    }

    const onDataLoaded = (data) => {
        setData(data);
    }

    return (
        <>
            <AppBanner/>
            {setContent(process, Component, data)}
        </>
    )
}
export default SinglePageLayout;
