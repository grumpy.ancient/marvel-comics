import {useState} from "react";
import {Formik, Field, Form, ErrorMessage as FormikErrorMessage} from 'formik';
import {Link} from "react-router-dom"
import * as Yup from 'yup';

import useMarvelService from "../../services/MarvelService";
import ErrorMessage from "../errorMessage/ErrorMessage";

import "./searchCharacters.scss";
import Spinner from "../spinner/Spinner";

const SearchCharacters = () => {
    const [char, setChar] = useState(null);
    const {getCharacterByName, process, setProcess, clearError} = useMarvelService();

    const onRequest = (name) => {
        clearError();
        getCharacterByName(name)
            .then(onCharLoaded)
            .then(() => setProcess('success'))
    }

    const onCharLoaded = (char) => {
        setChar(char);
    }

    const setContent = () => {
        switch (process) {
            case 'waiting':
                return null
            case 'loading':
                return <Spinner/>
            case 'error':
                return <div className="char__search-critical-error"><ErrorMessage/></div>;
            case 'success':
                return char.length > 0 ?
                    <div className="char__search-wrapper">
                        <div className="char__search-success">There is! Visit {char[0].name} page?</div>
                        <Link to={`/characters/${char[0].id}`} className="button button__secondary">
                            <div className="inner">To page</div>
                        </Link>
                    </div> :
                    <div className="char__search-error">
                        The character was not found. Check the name and try again
                    </div>
            default:
                throw new Error('Unexpected process state');
        }
    }

    return (
        <div className="char__search-form">
            <Formik
                initialValues={{charName: ''}}
                validationSchema={Yup.object({
                    charName: Yup.string().required('This Field is required')
                })}
                onSubmit={({charName}) => {
                    onRequest(charName)
                }}
            >
                <Form>
                    <label className="char__search-label" htmlFor="name">Or find character by name:</label>
                    <div className="char__search-wrapper">
                        <Field
                            id="charName"
                            name="charName"
                            type="text"
                            placeholder="Enter name"/>
                        <button
                            type="submit"
                            aria-label="find"
                            className="button button__main"
                            disabled={process === 'loading'}>
                            <div className="inner">Find</div>
                        </button>
                    </div>
                    <FormikErrorMessage name="charName" component="div" className="char__search-error"/>
                </Form>
            </Formik>
            {setContent()}
        </div>
    )
}
export default SearchCharacters;