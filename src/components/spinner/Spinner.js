const Spinner = () => {
    return (
        <svg xmlns="http://www.w3.org/2000/svg"
             style={{margin: 'auto', background: 'rgb(255, 255, 255)', display: 'block', shapeRendering: 'auto'}} width="197px"
             height="197px" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid">
            <circle cx="50" cy="50" r="27" strokeWidth="5" stroke="#9f0013"
                    strokeDasharray="42.411500823462205 42.411500823462205" fill="none" strokeLinecap="round">
                <animateTransform attributeName="transform" type="rotate" repeatCount="indefinite"
                                  dur="1.075268817204301s" keyTimes="0;1" values="0 50 50;360 50 50">

                </animateTransform>
            </circle>
        </svg>
    )
}
export default Spinner;