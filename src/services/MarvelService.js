import useHttp from '../hook/http.hook'

const useMarvelService = () => {
    const {process, setProcess, request, clearError} = useHttp();

    const _apiBase = ' https://gateway.marvel.com:443/v1/public/';
    // const _apiKey = 'apikey=35cec255ad3b1b6955470752e02a47d6';//v.sav4ik@gmail.com
    const _apiKey = 'apikey=c76764f5810333cb8a84483ae42cc442'; //grumpy.ancient@gmail.com
    const _startOffsetCharacter = 1344;
    const _startOffsetComics = 0;


    const getAllCharacters = async (offset = _startOffsetCharacter) => {
        const res = await request(`${_apiBase}characters?limit=9&offset=${offset}&${_apiKey}`);
        return res.data.results.map(_transformCharacter);
    }

    const getCharacter = async (id) => {
        const res = await request(`${_apiBase}characters/${id}?${_apiKey}`);
        return _transformCharacter(res.data.results[0]);
    }

    const getCharacterByName = async (name) => {
        const res = await request(`${_apiBase}characters?name=${name}&${_apiKey}`);
        return res.data.results.map(_transformCharacter);
    }

    const getAllComics = async (offset = _startOffsetComics) => {
        const res = await request(`${_apiBase}comics?limit=8&offset=${offset}&${_apiKey}`);
        return res.data.results.map(_transformComics);
    }

    const getComic = async (id) => {
        const res = await request(`${_apiBase}comics/${id}?${_apiKey}`);
        return _transformComics(res.data.results[0]);
    }

    const _transformCharacter = (char) => {
        return {
            id: char.id,
            name: char.name,
            description: char.description ? char.description.slice(0, 140) + '...' : 'This character don\'t have description',
            descriptionFull: char.description ? char.description : 'This character don\'t have description',
            thumbnail: char.thumbnail.path + '.' + char.thumbnail.extension,
            homepage: char.urls[0].url,
            wiki: char.urls[1].url,
            comics: char.comics.items
        };
    }

    const _transformComics = (comics) => {
        return {
            id: comics.id,
            title: comics.title,
            thumbnail: comics.thumbnail.path + '.' + comics.thumbnail.extension,
            price: comics.prices[0].price ? `${comics.prices[0].price}$` : 'not available',
            description: comics.description || 'This character don\'t have description',
            pageCount: comics.pageCount ? `${comics.pageCount} p.` : 'No information about the number of pages',
            language: comics.textObjects.language || "en-us"
        };
    }

    return {
        clearError,
        process,
        setProcess,
        getAllCharacters,
        getCharacter,
        getCharacterByName,
        getAllComics,
        getComic
    }
}

export default useMarvelService;

